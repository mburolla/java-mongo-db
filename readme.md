# Java Mongo DB
Helper project that shows how to read/write from MongoDB.

# Links
[Getting Started with Mongo](https://www.mongodb.com/blog/post/getting-started-with-mongodb-and-java-part-i)

[Install MongoDB Community Edition](https://www.mongodb.com/try/download/community?tck=docs_server) 

# Notes
Built using IntelliJ IDEA 2021.2.2 (Community Edition)
