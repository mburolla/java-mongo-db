package com.xpanxion.mongodb;

import com.xpanxion.mongodb.models.Person;
import com.xpanxion.mongodb.DAO.DataAccess;
import com.xpanxion.mongodb.models.Address;

import java.net.UnknownHostException;

public class Main {
    public static void main(String[] args) throws UnknownHostException {
        var dataAccess = new DataAccess();
//        var person1 = new Person(123, "Alice", new Address("Rochester", "NY", 14519, "123 Red Street"));
//        var person2 = new Person(456, "Bob", new Address("Rochester", "NY", 14580, "456 Green Street"));
//        var person3 = new Person(789, "Charlie", new Address("Rochester", "NY", 14519, "789 Blue Street"));
        var person4 = new Person(222, "Dave", new Address("Rochester", "NY", 14519, "test"), "111-11-1111");

//        dataAccess.insertPerson(person1);
//        dataAccess.insertPerson(person2);
//        dataAccess.insertPerson(person3);
        //dataAccess.insertPerson(person4);

        var person = dataAccess.findPerson(222);
        System.out.println(person);
    }
}
