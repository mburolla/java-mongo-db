package com.xpanxion.mongodb.DAO;

import com.mongodb.*;
import com.xpanxion.mongodb.models.Address;
import com.xpanxion.mongodb.models.Person;

import java.net.UnknownHostException;

/**
 * Manually convert POJOs to MongoDB Objects and vice-versa.
 */
public class DataAccess {

    //
    // Data Members
    //

    final String DB_NAME = "MartyDB";
    final String COLLECTION_NAME = "person";
    final String MONGO_CLIENT_URI = "mongodb://localhost:27017";

    private DBCollection mongoPersonCollection;

    //
    // Constructors
    //

    public DataAccess() {
        try {
            var mongoClient = new MongoClient(new MongoClientURI(MONGO_CLIENT_URI));
            var mongoDB = mongoClient.getDB(DB_NAME);
            mongoPersonCollection = mongoDB.getCollection(COLLECTION_NAME);
        }
        catch(UnknownHostException uhe) {
            uhe.printStackTrace();
        }
    }

    //
    // Methods
    //

    public void insertPerson(Person person) {
        var address = new BasicDBObject("street", person.getAddress().getStreet())
                .append("city",  person.getAddress().getCity())
                .append("state", person.getAddress().getState())
                .append("zip", person.getAddress().getZip());

        DBObject dbPerson = new BasicDBObject("_id", person.getId())
                .append("name", person.getName())
                .append("address", address)
                .append("ssn", person.getSsn());

        mongoPersonCollection.insert(dbPerson);
    }

    public Person findPerson(Integer id) {
        Person retval = null;

        DBObject query = new BasicDBObject("_id", id);
        DBCursor cursor = mongoPersonCollection.find(query);

        var o = cursor.one();
        var name = (String)o.get("name");
        var a = (DBObject)o.get("address");
        var ssn = (String)o.get("ssn");

        var street = (String)a.get("street");
        var city = (String)a.get("city");
        var state = (String)a.get("state");
        var zip = (Integer)a.get("zip");

        retval = new Person(id, name, new Address(city, state, zip, street), ssn);
        return retval;
    }
}
