package com.xpanxion.mongodb.models;

public class Person {

    //
    // Data members
    //

    private Integer id;
    private String name;
    private Address address;
    private String ssn;

    //
    // Constructors
    //

    public Person(Integer id, String name, Address address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Person(Integer id, String name, Address address, String ssn) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.ssn = ssn;
    }

    //
    // Accessors
    //

    public String getSsn() {
        return ssn;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address=" + address +
                ", ssn='" + ssn + '\'' +
                '}';
    }
}
