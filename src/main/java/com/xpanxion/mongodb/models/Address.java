package com.xpanxion.mongodb.models;

public class Address {

    //
    // Data members
    //

    private Integer zip;
    private String city;
    private String state;
    private String street;

    //
    // Constructors
    //

    public Address(String city, String state, Integer zip, String street) {
        this.zip = zip;
        this.city = city;
        this.state = state;
        this.street = street;
    }

    //
    // Accessors
    //

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getZip() {
        return zip;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Address{" +
                "zip=" + zip +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", street='" + street + '\'' +
                '}';
    }
}
